package com.github.welblade.controleveiculos.controller.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@SpringBootTest
@AutoConfigureMockMvc
public class VeiculoResourceTest {
    
    @Autowired 
    private MockMvc mvc;

    @Test
    public void cadastrarUmVeiculoRetornoOK() throws Exception{

        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> veiculo = new HashMap<String, String>();

        veiculo.put("marca", "Asia Motors");
        veiculo.put("modelo", "AM-825 Luxo 4.0 Diesel");
        veiculo.put("ano", "1997");

        final ResultActions result =
                mvc.perform(
                    post("/veiculo")
                        .content(mapper.writeValueAsString(veiculo))
                        .contentType(MediaType.APPLICATION_JSON));
        
        result.andExpect(status().isCreated());        
    }

    @Test
    public void cadastrarUmVeiculoComCpfRetornoOK() throws Exception{

        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> veiculo = new HashMap<String, String>();

        veiculo.put("cpf", "75327045056");
        veiculo.put("marca", "VW - VolksWagen");
        veiculo.put("modelo", "AMAROK CS2.0 16V/S2.0 16V TDI 4x2 Diesel");
        veiculo.put("ano", "2014");

        final ResultActions result =
                mvc.perform(
                    post("/veiculo")
                        .content(mapper.writeValueAsString(veiculo))
                        .contentType(MediaType.APPLICATION_JSON));
        
        result.andExpect(status().isCreated());        
    }

    @Test    
    public void cadastrarUmVeiculoExistenteFalha() throws Exception{

        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> veiculo = new HashMap<String, String>();

        veiculo.put("marca", "Asia Motors");
        veiculo.put("modelo", "AM-825 Luxo 4.0 Diesel");
        veiculo.put("ano", "1998");

        final ResultActions result =
                mvc.perform(
                    post("/veiculo")
                        .content(mapper.writeValueAsString(veiculo))
                        .contentType(MediaType.APPLICATION_JSON));
        
        final ResultActions result2 =
                mvc.perform(
                    post("/veiculo")
                        .content(mapper.writeValueAsString(veiculo))
                        .contentType(MediaType.APPLICATION_JSON));
        
        result.andExpect(status().isCreated()); 
        result2.andExpect(status().isConflict());        
    }

    @Test    
    public void cadastrarUmVeiculoComCPFUDeUsuarioNaoExtistenteDeveFalhar() throws Exception{

        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> veiculo = new HashMap<String, String>();
        veiculo.put("cpf", "90274132079");
        veiculo.put("marca", "Asia Motors");
        veiculo.put("modelo", "AM-825 Luxo 4.0 Diesel");
        veiculo.put("ano", "1998");

        final ResultActions result =
                mvc.perform(
                    post("/veiculo")
                        .content(mapper.writeValueAsString(veiculo))
                        .contentType(MediaType.APPLICATION_JSON));
        
        System.out.println(result.andReturn().getResponse().getContentAsString());
        result.andExpect(status().isNotFound()); 
    }

    @Test    
    public void cadastrarUmVeiculoComCPFUDeUsuarioInvalidoDeveFalhar() throws Exception{

        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> veiculo = new HashMap<String, String>();
        veiculo.put("cpf", "9027413207");
        veiculo.put("marca", "Asia Motors");
        veiculo.put("modelo", "AM-825 Luxo 4.0 Diesel");
        veiculo.put("ano", "1998");

        final ResultActions result =
                mvc.perform(
                    post("/veiculo")
                        .content(mapper.writeValueAsString(veiculo))
                        .contentType(MediaType.APPLICATION_JSON));
        
        System.out.println(result.andReturn().getResponse().getContentAsString());
        result.andExpect(status().isBadRequest()); 
    }

    @Test    
    public void cadastrarUmVeiculoComMarcaInexistenteDeveFalhar() throws Exception{

        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> veiculo = new HashMap<String, String>();
        veiculo.put("cpf", "75327045056");
        veiculo.put("marca", "Asiatica Motors");
        veiculo.put("modelo", "AM-825 Luxo 4.0 Diesel");
        veiculo.put("ano", "1998");

        final ResultActions result =
                mvc.perform(
                    post("/veiculo")
                        .content(mapper.writeValueAsString(veiculo))
                        .contentType(MediaType.APPLICATION_JSON));
        
        System.out.println(result.andReturn().getResponse().getContentAsString());
        result.andExpect(status().isBadRequest()); 
    }

    @Test
    public void cadastrarUmVeiculoComModeloInexistenteDeveFalhar() throws Exception{

        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> veiculo = new HashMap<String, String>();
        veiculo.put("cpf", "75327045056");
        veiculo.put("marca", "Asia Motors");
        veiculo.put("modelo", "AM-825 Lixo 4.0 Diesel");
        veiculo.put("ano", "1998");

        final ResultActions result =
                mvc.perform(
                    post("/veiculo")
                        .content(mapper.writeValueAsString(veiculo))
                        .contentType(MediaType.APPLICATION_JSON));
        
        System.out.println(result.andReturn().getResponse().getContentAsString());
        result.andExpect(status().isBadRequest()); 
    }

    @Test
    public void cadastrarUmVeiculoComAnoInexistenteDeveFalhar() throws Exception{

        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> veiculo = new HashMap<String, String>();
        veiculo.put("cpf", "75327045056");
        veiculo.put("marca", "Asia Motors");
        veiculo.put("modelo", "AM-825 Luxo 4.0 Diesel");
        veiculo.put("ano", "1888");

        final ResultActions result =
                mvc.perform(
                    post("/veiculo")
                        .content(mapper.writeValueAsString(veiculo))
                        .contentType(MediaType.APPLICATION_JSON));
        
        System.out.println(result.andReturn().getResponse().getContentAsString());
        result.andExpect(status().isBadRequest()); 
    }
}
