package com.github.welblade.controleveiculos.controller.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@SpringBootTest
@AutoConfigureMockMvc
public class UsuarioResourceTest {
    
    @Autowired 
    private MockMvc mvc;

    @Test
    public void cadastrarUmUsuarioRetornoOK() throws Exception{

        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> usuario = new HashMap<String, String>();

        usuario.put("cpf", "18577692019");
        usuario.put("email", "hhadassaelisaandreiaduarte@jerasistemas.com.br");
        usuario.put("nome", "Hadassa Elisa Andreia Duarte");
        usuario.put("dataNascimento", "1959-02-23");

        final ResultActions result =
                mvc.perform(
                    post("/usuario")
                        .content(mapper.writeValueAsString(usuario))
                        .contentType(MediaType.APPLICATION_JSON));
        
        result.andExpect(status().isCreated());        
    }

    @Test
    public void cadastrarUmUsuarioComErro() throws Exception{

        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> usuario = new HashMap<String, String>();

        usuario.put("cpf", "1857769201");
        usuario.put("email", "hhadassaelisaandreiaduarte@jerasistemas.com.br");
        usuario.put("nome", "Hadassa Elisa Andreia Duarte");
        usuario.put("dataNascimento", "1959-02-23");

        final ResultActions result =
                mvc.perform(
                    post("/usuario")
                        .content(mapper.writeValueAsString(usuario))
                        .contentType(MediaType.APPLICATION_JSON));
        
        result.andExpect(status().isBadRequest());        
    }
}
