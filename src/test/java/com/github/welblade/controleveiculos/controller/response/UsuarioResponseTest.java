package com.github.welblade.controleveiculos.controller.response;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.github.welblade.controleveiculos.dto.model.veiculo_usuario.VeiculoUsuarioDto;
import com.github.welblade.controleveiculos.repository.usuario.UsuarioRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestComponent;

@TestComponent
public class UsuarioResponseTest {
    @Autowired
    UsuarioRepository usuarioRepository;

    @Test
    public void quantoCriadoDeveConterPropriedadesEsperadas(){
        List<VeiculoUsuarioDto> veiculos = new ArrayList<VeiculoUsuarioDto>();

        UsuarioResponse usuario = new UsuarioResponse();
        usuario.setCPF("91190098792");
        usuario.setEmail("mariabeneditabarbosa@cntbrasil.com.br");
        usuario.setNome("Maria Benedita Barbosa");
        usuario.setDataNascimento("1979-10-05");
        usuario.setVeiculos(veiculos);

        assertAll(
            () -> assertEquals("91190098792", usuario.getCPF()),
            () -> assertEquals("mariabeneditabarbosa@cntbrasil.com.br", usuario.getEmail()),
            () -> assertEquals("Maria Benedita Barbosa", usuario.getNome()),
            () -> assertEquals("1979-10-05", usuario.getDataNascimento()),
            () -> assertEquals(veiculos, usuario.getVeiculos())
        );
    }
}
