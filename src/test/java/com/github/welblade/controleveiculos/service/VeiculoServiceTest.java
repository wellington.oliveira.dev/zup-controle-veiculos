package com.github.welblade.controleveiculos.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import com.github.welblade.controleveiculos.dto.mapper.VeiculoMapper;
import com.github.welblade.controleveiculos.dto.model.veiculo.VeiculoDto;
import com.github.welblade.controleveiculos.exception.ApiFipeNenhumaMarcaEncontradaException;
import com.github.welblade.controleveiculos.exception.CPFUsuarioNaoCadastradoException;
import com.github.welblade.controleveiculos.exception.VeiculoJaCadastradoException;
import com.github.welblade.controleveiculos.model.veiculo.Veiculo;
import com.github.welblade.controleveiculos.model.veiculo_usuario.VeiculoUsuario;
import com.github.welblade.controleveiculos.repository.veiculo.VeiculoRepository;
import com.github.welblade.controleveiculos.repository.veiculo_usuario.VeiculoUsuarioRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

@SpringBootTest
public class VeiculoServiceTest {

    @Autowired
    VeiculoService service;

    @Autowired
    VeiculoUsuarioRepository repositorio;

    @Autowired
    VeiculoRepository repositorioVeiculo;

    @Transactional
    @Rollback(true)
    @Test
    public void quandoCadastrarSemCPFRetornarVeiculo(){
        VeiculoDto veiculo = new VeiculoDto();
        // veiculo.setCPF("91190098792");
        veiculo.setMarca("VW - VolksWagen");
        veiculo.setModelo("AMAROK CS2.0 16V/S2.0 16V TDI 4x2 Diesel");
        veiculo.setAno("2014");

        Veiculo veiculoResultado = service.registrar(veiculo);


        assertAll(
            () -> assertEquals("VW - VolksWagen", veiculoResultado.getMarca()),
            () -> assertEquals("AMAROK CS2.0 16V/S2.0 16V TDI 4x2 Diesel", veiculoResultado.getModelo()),
            () -> assertEquals("2014", veiculoResultado.getAno()),
            () -> assertEquals("R$ 68.250,00", veiculoResultado.getValor())
        );
    }

    @Transactional
    @Rollback(true)
    @Test
    public void quandoCadastrarComMarcaIncompletaRetornarVeiculo(){
        VeiculoDto veiculo = new VeiculoDto();
        // veiculo.setCPF("91190098792");
        veiculo.setMarca("VolksWagen");
        veiculo.setModelo("AMAROK CS2.0 16V/S2.0 16V TDI 4x2 Diesel");
        veiculo.setAno("2014");

        Veiculo veiculoResultado = service.registrar(veiculo);


        assertAll(
            () -> assertEquals("VW - VolksWagen", veiculoResultado.getMarca()),
            () -> assertEquals("AMAROK CS2.0 16V/S2.0 16V TDI 4x2 Diesel", veiculoResultado.getModelo()),
            () -> assertEquals("2014", veiculoResultado.getAno()),
            () -> assertEquals("R$ 68.250,00", veiculoResultado.getValor())
        );
    }

    @Transactional
    @Rollback(true)
    @Test
    public void quandoCadastrarComModeloIncompletoRetornarVeiculo(){
        VeiculoDto veiculo = new VeiculoDto();
        // veiculo.setCPF("91190098792");
        veiculo.setMarca("VolksWagen");
        veiculo.setModelo("AMAROK CS2.0");
        veiculo.setAno("2014");

        Veiculo veiculoResultado = service.registrar(veiculo);


        assertAll(
            () -> assertEquals("VW - VolksWagen", veiculoResultado.getMarca()),
            () -> assertEquals("AMAROK CS2.0 16V/S2.0 16V TDI 4x2 Diesel", veiculoResultado.getModelo()),
            () -> assertEquals("2014", veiculoResultado.getAno()),
            () -> assertEquals("R$ 68.250,00", veiculoResultado.getValor())
        );
    }

    @Transactional
    @Rollback(true)
    @Test
    public void quandoCadastrarComCPFRetornarVeiculo(){
        VeiculoDto veiculo = new VeiculoDto();
        veiculo.setCPF("55980294058");
        veiculo.setMarca("VW - VolksWagen");
        veiculo.setModelo("AMAROK CS2.0 16V/S2.0 16V TDI 4x2 Diesel");
        veiculo.setAno("2014");

        Veiculo veiculoResultado = service.registrar(veiculo);

        List<VeiculoUsuario> veiculosUsuario = repositorio.findByVeiculoId(veiculoResultado.getId());
        
        
        assertAll(
            () -> assertEquals("55980294058", veiculo.getCPF()),
            () -> assertEquals("VW - VolksWagen", veiculoResultado.getMarca()),
            () -> assertEquals("AMAROK CS2.0 16V/S2.0 16V TDI 4x2 Diesel", veiculoResultado.getModelo()),
            () -> assertEquals("2014", veiculoResultado.getAno()),
            () -> assertEquals("R$ 68.250,00", veiculoResultado.getValor()),
            () -> assertListVeiculosContainsCPF("55980294058", veiculosUsuario)
        );
    }

    private void assertListVeiculosContainsCPF(String expectedCPF, List<VeiculoUsuario> veiculosUsuario){
        boolean found = false;
        for(var veiculo: veiculosUsuario){
            System.out.println("Saida: " + veiculo.getProprietario().getCPF());
            if(veiculo.getProprietario().getCPF() == expectedCPF){
                found = true;
            }
        }
        assertTrue(found);
    }

    @Transactional
    @Rollback(true)
    @Test
    public void quandoCadastrarComCPFNaoExistenteDeveFalhar(){
        VeiculoDto veiculo = new VeiculoDto();
        veiculo.setCPF("28452769075");
        veiculo.setMarca("VW - VolksWagen");
        veiculo.setModelo("AMAROK High.CD 2.0 16V TDI 4x4 Dies. Aut");
        veiculo.setAno("2014");

        CPFUsuarioNaoCadastradoException exception = assertThrows(
            CPFUsuarioNaoCadastradoException.class,
             () ->  service.registrar(veiculo)
        );

        assertTrue(exception.getMessage()
            .contains("Não existe um usuário com este CPF cadastrado em nosso sistema, por favor cadastre-o primeiro.")
        );
    }

    @Transactional
    @Rollback(true)
    @Test
    public void quandoCadastrarComCPFInvalidoDeveFalhar(){
        VeiculoDto veiculo = new VeiculoDto();
        veiculo.setCPF("2845276907");
        veiculo.setMarca("VW - VolksWagen");
        veiculo.setModelo("AMAROK High.CD 2.0 16V TDI 4x4 Dies. Aut");
        veiculo.setAno("2014");

        ConstraintViolationException exception = assertThrows(
            ConstraintViolationException.class,
             () ->  service.registrar(veiculo)
        );
        assertTrue(exception.getMessage()
            .contains("número do registro de contribuinte individual brasileiro (CPF) inválido")
        );
    }

    @Transactional
    @Rollback(true)
    @Test
    public void quandoCadastrarComCPFVazioDeveFalhar(){
        VeiculoDto veiculo = new VeiculoDto();
        veiculo.setCPF("");
        veiculo.setMarca("VW - VolksWagen");
        veiculo.setModelo("AMAROK High.CD 2.0 16V TDI 4x4 Dies. Aut");
        veiculo.setAno("2014");

        ConstraintViolationException exception = assertThrows(
            ConstraintViolationException.class,
             () ->  service.registrar(veiculo)
        );
        System.out.println("ERRO :: " + exception.getMessage());
        assertTrue(exception.getMessage()
            .contains("número do registro de contribuinte individual brasileiro (CPF) inválido")
        );
    }

    @Transactional
    @Rollback(true)
    @Test
    public void quandoCadastrarComMarcaInexistenteFalhar(){
        VeiculoDto veiculo = new VeiculoDto();
        veiculo.setMarca("Xana");
        veiculo.setModelo("AMAROK High.CD 2.0 16V TDI 4x4 Dies. Aut");
        veiculo.setAno("2014");

        ApiFipeNenhumaMarcaEncontradaException exception = assertThrows(
            ApiFipeNenhumaMarcaEncontradaException.class,
             () ->  service.registrar(veiculo)
        );
        System.out.println("ERRO :: " + exception.getMessage());
        assertTrue(exception.getMessage()
            .contains("Nenhuma marca")
        );
    }

    @Transactional
    @Rollback(true)
    @Test
    public void quandoCadastrarComMarcaExistenteFalha(){
        List<Veiculo> veiculosAntes = repositorioVeiculo.findAll();
        VeiculoDto veiculo = VeiculoMapper.toDto(veiculosAntes.get(0));
        assertThrows(VeiculoJaCadastradoException.class, () -> service.registrar(veiculo) );
    }

}
