package com.github.welblade.controleveiculos.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.validation.ConstraintViolationException;

import com.github.welblade.controleveiculos.dto.model.usuario.UsuarioDto;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
public class UsuarioServiceTest {
    @Autowired
    UsuarioService service;


    @Test
    @Transactional
    @Rollback(true)
    public void quandoRegistrarRetornarOUsuario(){
        UsuarioDto usuario = new UsuarioDto()
            .setCPF("12867493013")
            .setEmail("mariabeneditabarbosa@cntbrasil.com.br")
            .setNome("Maria Benedita Barbosa")
            .setDataNascimento("1979-10-05");

        UsuarioDto usuarioRetorno = service.registrar(usuario);
        assertAll(
            () -> assertEquals(UsuarioDto.class, usuarioRetorno.getClass()),
            () -> assertEquals(usuario.getCPF(), usuarioRetorno.getCPF()),
            () -> assertEquals(usuario.getEmail(), usuarioRetorno.getEmail()),
            () -> assertEquals(usuario.getNome(), usuarioRetorno.getNome()),
            () -> assertEquals(usuario.getDataNascimento(), usuarioRetorno.getDataNascimento())
        );
    }

    @Test
    @Transactional
    @Rollback(true)
    public void quandoCPFExistenteLancarExcessao(){
        UsuarioDto usuario = new UsuarioDto()
            .setCPF("12867493013")
            .setEmail("mariabeneditabarbosa1@cntbrasil.com.br")
            .setNome("Maria Benedita Barbosa")
            .setDataNascimento("1979-10-05");
        
        UsuarioDto usuario2 = new UsuarioDto()
            .setCPF("12867493013")
            .setEmail("mariabeneditabarbosa2@cntbrasil.com.br")
            .setNome("Maria Benedita Barbosa")
            .setDataNascimento("1979-10-05");
        
        service.registrar(usuario);

        ConstraintViolationException exception = assertThrows(
            ConstraintViolationException.class,
             () -> service.registrar(usuario2)
        );
        assertTrue(exception.getMessage()
            .contains("Este CPF já está cadastrado em nosso sistema.")
        );
        
    }

    @ParameterizedTest
    @ValueSource(strings = {"89898213045", "898982130453", "0"}) 
    public void quandoCPFInvalidoLancarExcessao(String cpf){
        UsuarioDto usuario = new UsuarioDto()
            .setCPF("89898213045")
            .setEmail("mariabeneditabarbosa4@cntbrasil.com.br")
            .setNome("Maria Benedita Barbosa")
            .setDataNascimento("1979-10-05");

        ConstraintViolationException exception = assertThrows(
            ConstraintViolationException.class, 
            () -> service.registrar(usuario)
        );
        assertTrue(exception.getMessage().contains("CPF Inválido"));  
    }

    @Test
    @Transactional
    @Rollback(true)
    public void quandoEmailExistenteLancarExcessao(){
        UsuarioDto usuario = new UsuarioDto()
            .setCPF("78694021099")
            .setEmail("mariabeneditabarbosa3@cntbrasil.com.br")
            .setNome("Maria Benedita Barbosa")
            .setDataNascimento("1979-10-05");
        
        UsuarioDto usuario2 = new UsuarioDto()
            .setCPF("99812657053")
            .setEmail("mariabeneditabarbosa3@cntbrasil.com.br")
            .setNome("Maria Benedita Barbosa")
            .setDataNascimento("1979-10-05");
        
        service.registrar(usuario);

        ConstraintViolationException exception = assertThrows(
            ConstraintViolationException.class, 
            () -> service.registrar(usuario2)
        );
        assertTrue(exception.getMessage()
            .contains("Este Email já está cadastrado em nosso sistema.")
        );
    }

    @Test
    public void quandoEmailEmBrancoLancarExcessao(){
        UsuarioDto usuario = new UsuarioDto()
            .setCPF("78694021099")
            .setEmail("")
            .setNome("Maria Benedita Barbosa")
            .setDataNascimento("1979-10-05");
        
        ConstraintViolationException exception = assertThrows(
            ConstraintViolationException.class, 
            () -> service.registrar(usuario)
        );
        assertTrue(exception.getMessage()
            .contains("O Email não pode estar em branco.")
        );
    }

    @Test
    public void quandoEmailNuloLancarExcessao(){
        UsuarioDto usuario = new UsuarioDto()
            .setCPF("78694021099")
            .setEmail(null)
            .setNome("Maria Benedita Barbosa")
            .setDataNascimento("1979-10-05");
        
        ConstraintViolationException exception = assertThrows(
            ConstraintViolationException.class, 
            () -> service.registrar(usuario)
        );
        assertTrue(exception.getMessage()
            .contains("O Email não pode ser um valor nulo.")
        );
    }

    @ParameterizedTest
    @ValueSource(strings = {"mariabeneditabarbosa", "898982130453", "0", "mariabeneditabarbosamariabeneditabarbosamariabeneditabarbosamariabeneditabarbosamariabeneditabarbosamariabeneditabarbosamariabeneditabarbosamariabeneditabarbosamariabeneditabarbosamariabeneditabarbosa"}) 
    public void quandoEmailInvalidoExcessao(String email){
        UsuarioDto usuario = new UsuarioDto()
            .setCPF("78694021099")
            .setEmail(email)
            .setNome("Maria Benedita Barbosa")
            .setDataNascimento("1979-10-05");
        ConstraintViolationException exception = assertThrows(
            ConstraintViolationException.class, 
            () -> service.registrar(usuario)
        );
        assertTrue(exception.getMessage()
            .contains("O email não possui um formato válido.")
        );
    }

    @Test
    public void quandoNomeNuloLancarExcessao(){
        UsuarioDto usuario = new UsuarioDto()
            .setCPF("78694021099")
            .setEmail("mariabeneditabarbosa3@cntbrasil.com.br")
            .setNome(null)
            .setDataNascimento("1979-10-05");
        ConstraintViolationException exception = assertThrows(
            ConstraintViolationException.class, 
            () -> service.registrar(usuario)
        );
        assertTrue(exception.getMessage()
            .contains("O Nome não pode ser um valor nulo.")
        );
    }

    @Test
    public void quandoNomeEmBrancoLancarExcessao(){
        UsuarioDto usuario = new UsuarioDto()
            .setCPF("78694021099")
            .setEmail("mariabeneditabarbosa3@cntbrasil.com.br")
            .setNome("")
            .setDataNascimento("1979-10-05");
        ConstraintViolationException exception = assertThrows(
            ConstraintViolationException.class, 
            () -> service.registrar(usuario)
        );
        assertTrue(exception.getMessage()
            .contains("O Nome não pode estar em branco.")
        );
    }

    @ParameterizedTest
    @ValueSource(strings = {"ab", "imuwsmweyijuusjresgkszqgyhaoqbrznnwgbrlzbwtwpeouxwfpuxxteyjwzcwnpruqjgpkdkhspwnmstdnqbiuzsryunmkfhcoosardhlsezgxtjlkkugknyjilwsrdwhrohkrpwluusrpceanzhzmgiurfokikrbaalsqheldxesaitoicouixqbcfllqenqezjwhohkmmenwzdebrzjpqkswwenbsffrpczejkrxpfsojyqbyaqolkl"}) 
    public void quandoNomeTamanhoInvalidoLancarExcessao(String nome){
        UsuarioDto usuario = new UsuarioDto()
            .setCPF("78694021099")
            .setEmail("mariabeneditabarbosa3@cntbrasil.com.br")
            .setNome(nome)
            .setDataNascimento("1979-10-05");
        ConstraintViolationException exception = assertThrows(
            ConstraintViolationException.class, 
            () -> service.registrar(usuario)
        );
        
        assertTrue(exception.getMessage()
            .contains("O comprimento do Nome deve ser entre 3 e 250.")
        );
    }
    @Test
    public void quandoNomeDataInvalidaLancarExcessao(){
        UsuarioDto usuario = new UsuarioDto()
            .setCPF("78694021099")
            .setEmail("mariabeneditabarbosa3@cntbrasil.com.br")
            .setNome("Maria Benedita")
            .setDataNascimento("19791005");
        ConstraintViolationException exception = assertThrows(
            ConstraintViolationException.class, 
            () -> service.registrar(usuario)
        );
        assertTrue(exception.getMessage()
            .contains("A data deve ter o formato YYYY-MM-DD")
        );
    }

}
