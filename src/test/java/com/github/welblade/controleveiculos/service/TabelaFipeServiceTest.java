package com.github.welblade.controleveiculos.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.github.welblade.controleveiculos.dto.model.veiculo.VeiculoDto;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootTest
public class TabelaFipeServiceTest {
    @Autowired
    TabelaFipeService service;

    @Test
    public void requisicaoRetornarValor(){
        VeiculoDto veiculo = new VeiculoDto();
        veiculo.setCPF("91190098792");
        veiculo.setMarca("VW - VolksWagen");
        veiculo.setModelo("AMAROK High.CD 2.0 16V TDI 4x4 Dies. Aut");
        veiculo.setAno("2014");
        veiculo.setValor("R$ 102.451,00");

        String valor = service.getValorVeiculo(veiculo);
        assertEquals(veiculo.getValor(), valor);
    }

}
