package com.github.welblade.controleveiculos.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import com.github.welblade.controleveiculos.controller.response.UsuarioResponse;
import com.github.welblade.controleveiculos.model.usuario.Usuario;
import com.github.welblade.controleveiculos.model.veiculo.Veiculo;
import com.github.welblade.controleveiculos.model.veiculo_usuario.VeiculoUsuario;
import com.github.welblade.controleveiculos.repository.usuario.UsuarioRepository;
import com.github.welblade.controleveiculos.repository.veiculo.VeiculoRepository;
import com.github.welblade.controleveiculos.repository.veiculo_usuario.VeiculoUsuarioRepository;
import com.github.welblade.controleveiculos.util.DateConvertUtil;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.annotation.Rollback;
//import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
public class UsuarioServiceResponseTest {

    @Autowired
    UsuarioService service;

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    VeiculoRepository veiculoRepository;

    @Autowired
    VeiculoUsuarioRepository veiculoUsuarioRepository;

    public void inicializar(){
        Date data = DateConvertUtil.convertToDate("1969-06-25");
        
        Usuario usuario = new Usuario()
            .setCPF("47613426416")
            .setEmail("augustofelipecarvalho@comunikapublicidade.com.br")
            .setNome("Augusto Felipe Carvalho")
            .setDataNascimento(data);
               
        Usuario usuarioResultado = usuarioRepository.save(usuario);

        Veiculo veiculo = new Veiculo()
            .setMarca("Asia Motors")
            .setModelo("AM-825 Luxo 4.0 Diesel")
            .setAno("1998")
            .setValor("R$ 22.529,00"); 
        
        Veiculo veiculoResultado = veiculoRepository.save(veiculo);

        VeiculoUsuario veiculoUsuario = new VeiculoUsuario()
            .setProprietario(usuarioResultado)
            .setVeiculo(veiculoResultado);
        veiculoUsuarioRepository.save(veiculoUsuario);
    }

    @Test
   // @Transactional
   // @Rollback(true)
    public void quandoRetornarUsuario(){
        inicializar();
        
        UsuarioResponse response = service.acharUmUsuario("47613426416");
        
        assertAll(
            () -> assertEquals("47613426416", response.getCPF()),
            () -> assertEquals("augustofelipecarvalho@comunikapublicidade.com.br", response.getEmail()),
            () -> assertEquals("Augusto Felipe Carvalho", response.getNome()),
            () -> assertEquals("1969-06-25", response.getDataNascimento()),
            () -> assertEquals("Asia Motors", response.getVeiculos().get(0).getMarca()),
            () -> assertEquals("AM-825 Luxo 4.0 Diesel", response.getVeiculos().get(0).getModelo()),
            () -> assertEquals("1998", response.getVeiculos().get(0).getAno()),
            () -> assertEquals("R$ 22.529,00", response.getVeiculos().get(0).getValor())
        );
    }
    
}
