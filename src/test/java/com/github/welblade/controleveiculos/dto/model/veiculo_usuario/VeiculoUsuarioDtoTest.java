package com.github.welblade.controleveiculos.dto.model.veiculo_usuario;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;

import com.github.welblade.controleveiculos.util.DateConvertUtil;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.TestComponent;

@TestComponent
public class VeiculoUsuarioDtoTest {
    @Test
    public void quandoCriadoDeveApresentarDadosCorretos(){
        // uma quarta feira
        Date hoje = DateConvertUtil.convertToDate("2021-06-16");
        VeiculoUsuarioDto dto = new VeiculoUsuarioDto();
        dto.setMarca("VW - VolksWagen");
        dto.setModelo("AMAROK High.CD 2.0 16V TDI 4x4 Dies. Aut");
        dto.setAno("2014");
        dto.setValor("R$ 102.451,00");
        dto.setIsRodizioAtivo(hoje, new RegrasRodizio());

        assertAll(
            () -> assertEquals("VW - VolksWagen", dto.getMarca()),
            () -> assertEquals("AMAROK High.CD 2.0 16V TDI 4x4 Dies. Aut", dto.getModelo()),
            () -> assertEquals("2014", dto.getAno()),
            () -> assertEquals("R$ 102.451,00", dto.getValor()),
            () -> assertTrue(dto.getIsRodizioAtivo())
        );
    }
}
