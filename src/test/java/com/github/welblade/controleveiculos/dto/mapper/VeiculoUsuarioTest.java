package com.github.welblade.controleveiculos.dto.mapper;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;

import com.github.welblade.controleveiculos.dto.model.veiculo_usuario.RegrasRodizio;
import com.github.welblade.controleveiculos.dto.model.veiculo_usuario.VeiculoUsuarioDto;
import com.github.welblade.controleveiculos.model.usuario.Usuario;
import com.github.welblade.controleveiculos.model.veiculo.Veiculo;
import com.github.welblade.controleveiculos.model.veiculo_usuario.VeiculoUsuario;
import com.github.welblade.controleveiculos.util.DateConvertUtil;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.TestComponent;

@TestComponent
public class VeiculoUsuarioTest {
  
    @Test
    public void quandoMapearDeveApresetarDadosEsperados(){
        Date data = DateConvertUtil.convertToDate("1979-05-13");

        Usuario usuario = new Usuario()
            .setCPF("76452151355")
            .setEmail("llucasbrunodasilva@engemed.com")
            .setNome("Lucas Bruno da Silva")
            .setDataNascimento(data);
        Veiculo veiculo = new Veiculo()
            .setMarca("VW - VolksWagen")
            .setModelo("AMAROK High.CD 2.0 16V TDI 4x4 Dies. Aut")
            .setAno("2014")
            .setValor("R$ 102.451,00");

        VeiculoUsuario vUsuario = new VeiculoUsuario()
            .setProprietario(usuario)
            .setVeiculo(veiculo);
        
        RegrasRodizio regras = new RegrasRodizio();
        DayOfWeek diaRodizio = regras.getDiaRodizio("2014");
        LocalDate hoje = LocalDate.now();
        boolean hojeEhDiaRodizio = hoje.getDayOfWeek() == diaRodizio;

        VeiculoUsuarioDto dto = VeiculoUsuarioMapper.toDto(vUsuario);

        assertAll(
            () -> assertEquals(veiculo.getMarca(), dto.getMarca()),
            () -> assertEquals(veiculo.getModelo(), dto.getModelo()),
            () -> assertEquals(veiculo.getAno(), dto.getAno()),
            () -> assertEquals(veiculo.getValor(), dto.getValor()),
            () -> assertEquals(hojeEhDiaRodizio, dto.getIsRodizioAtivo())
        );

    }
}
