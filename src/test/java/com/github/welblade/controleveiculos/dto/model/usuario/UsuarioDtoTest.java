package com.github.welblade.controleveiculos.dto.model.usuario;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.TestComponent;

@TestComponent
public class UsuarioDtoTest {
    @Test
    public void quandoCriadoDevePossuirAtributosCorretos(){
        UsuarioDto usuario = new UsuarioDto();
        usuario.setCPF("91190098792");
        usuario.setEmail("mariabeneditabarbosa.mariabeneditabarbosa@cntbrasil.com.br");
        usuario.setNome("Maria Benedita Barbosa");
        usuario.setDataNascimento("1979-10-05");

        assertAll(
            () -> assertEquals("91190098792", usuario.getCPF()),
            () -> assertEquals("mariabeneditabarbosa.mariabeneditabarbosa@cntbrasil.com.br", usuario.getEmail()),
            () -> assertEquals("Maria Benedita Barbosa", usuario.getNome()),
            () -> assertEquals("1979-10-05", usuario.getDataNascimento())
        );
    }
}
