package com.github.welblade.controleveiculos.dto.mapper;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.github.welblade.controleveiculos.dto.model.veiculo.VeiculoDto;
import com.github.welblade.controleveiculos.model.veiculo.Veiculo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.TestComponent;

@TestComponent
public class VeiculoMapperTest {
    @Test
    public void quantoConverterDtoParaEntidadeValoresDevemCoincidir(){

        VeiculoDto veiculoDto = new VeiculoDto()
            .setCPF("91190098792")
            .setMarca("Hyundai")
            .setModelo("Accent LS 4p")
            .setAno("1995")
            .setValor("R$ 4.799,00");    

        Veiculo veiculo = VeiculoMapper.toEntity(veiculoDto);

        assertAll(
            () -> assertEquals(veiculoDto.getMarca(), veiculo.getMarca()),
            () -> assertEquals(veiculoDto.getModelo(), veiculo.getModelo()),
            () -> assertEquals(veiculoDto.getAno(), veiculo.getAno()),
            () -> assertEquals(veiculoDto.getValor(), veiculo.getValor())
        );
    }
    
}
