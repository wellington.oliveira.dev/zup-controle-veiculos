package com.github.welblade.controleveiculos.dto.mapper;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import com.github.welblade.controleveiculos.dto.model.usuario.UsuarioDto;
import com.github.welblade.controleveiculos.model.usuario.Usuario;
import com.github.welblade.controleveiculos.util.DateConvertUtil;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.TestComponent;

@TestComponent
public class UsuarioMapperTest {
    @Test
    public void quantoConverterUsuarioParaDtoValoresDevemCoincidir(){
        Usuario usuario = new Usuario();
        usuario.setCPF("91190098792");
        usuario.setEmail("mariabeneditabarbosa.mariabeneditabarbosa@cntbrasil.com.br");
        usuario.setNome("Maria Benedita Barbosa");
                
        Date data = DateConvertUtil.convertToDate("1979-10-05");

        usuario.setDataNascimento(data);

        UsuarioDto dto = UsuarioMapper.toDto(usuario);

        assertAll(
            () -> assertEquals(usuario.getCPF(), dto.getCPF()),
            () -> assertEquals(usuario.getEmail(), dto.getEmail()),
            () -> assertEquals(usuario.getNome(), dto.getNome()),
            () -> assertEquals("1979-10-05", dto.getDataNascimento())
        );
    }

    @Test
    public void quantoConverterDtoParaEntidadeValoresDevemCoincidir(){
        Date data = DateConvertUtil.convertToDate("1979-10-05");

        UsuarioDto usuarioDto = new UsuarioDto()
            .setCPF("91190098792")
            .setEmail("mariabeneditabarbosa.mariabeneditabarbosa@cntbrasil.com.br")
            .setNome("Maria Benedita Barbosa")
            .setDataNascimento("1979-10-05");    

        Usuario usuario = UsuarioMapper.toEntity(usuarioDto);

        assertAll(
            () -> assertEquals(usuarioDto.getCPF(), usuario.getCPF()),
            () -> assertEquals(usuarioDto.getEmail(), usuario.getEmail()),
            () -> assertEquals(usuarioDto.getNome(), usuario.getNome()),
            () -> assertEquals(data, usuario.getDataNascimento())
        );
    }
    
}
