package com.github.welblade.controleveiculos.dto.model.veiculo;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.TestComponent;

@TestComponent
public class VeiculoDtoRequestTest {
    VeiculoDto veiculo;

    @BeforeEach
    public void instanciarObjeto() {
        veiculo = new VeiculoDto();
    }

    @Test
    public void quandoCriadoDevePossuirAtributosCorretos(){
        veiculo.setCPF("91190098792");
        veiculo.setMarca("VW - VolksWagen");
        veiculo.setModelo("AMAROK High.CD 2.0 16V TDI 4x4 Dies. Aut");
        veiculo.setAno("2014");
        veiculo.setValor("R$ 102.451,00");


        assertAll(
            () -> assertEquals("91190098792", veiculo.getCPF()),
            () -> assertEquals("VW - VolksWagen", veiculo.getMarca()),
            () -> assertEquals("AMAROK High.CD 2.0 16V TDI 4x4 Dies. Aut", veiculo.getModelo()),
            () -> assertEquals("2014", veiculo.getAno()),
            () -> assertEquals("R$ 102.451,00", veiculo.getValor())
           
        );
    }
}