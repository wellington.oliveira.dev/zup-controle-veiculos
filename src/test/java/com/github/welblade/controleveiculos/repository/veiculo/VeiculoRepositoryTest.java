package com.github.welblade.controleveiculos.repository.veiculo;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import javax.transaction.Transactional;

import com.github.welblade.controleveiculos.model.veiculo.Veiculo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

@SpringBootTest
public class VeiculoRepositoryTest {
    @Autowired
    VeiculoRepository veiculoRepository;

    @Test
    @Transactional
    @Rollback(true)
    public void quandoCadastrarRetornarObjeto(){
        
        Veiculo veiculo = new Veiculo()
            .setMarca("Hyundai")
            .setModelo("Accent LS 4p")
            .setAno("1995")
            .setValor("R$ 4.799,00");    
        
        Veiculo veiculoResultado = veiculoRepository.save(veiculo);

        assertAll(
            () -> assertNotNull(veiculoResultado),
            () -> assertEquals(Veiculo.class, veiculoResultado.getClass()),
            () -> assertEquals(veiculo.getMarca(), veiculoResultado.getMarca()),
            () -> assertEquals(veiculo.getModelo(), veiculoResultado.getModelo()),
            () -> assertEquals(veiculo.getAno(), veiculoResultado.getAno()),
            () -> assertEquals(veiculo.getValor(), veiculoResultado.getValor())
        );
    }
}
