package com.github.welblade.controleveiculos.repository.usuario;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Date;

import javax.transaction.Transactional;

import com.github.welblade.controleveiculos.model.usuario.Usuario;
import com.github.welblade.controleveiculos.util.DateConvertUtil;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

@SpringBootTest
public class UsuatioRepositoryTest {
    @Autowired
    UsuarioRepository usuarioRepository;

    @Test
    @Transactional
    @Rollback(true)
    public void quandoCadastrarUsuarioRetornarUsuario(){
        Date data = DateConvertUtil.convertToDate("1979-10-05");
        
        Usuario usuario = new Usuario()
            .setCPF("91190098792")
            .setEmail("mariabeneditabarbosa.mariabeneditabarbosa@cntbrasil.com.br")
            .setNome("Maria Benedita Barbosa")
            .setDataNascimento(data);
                
        
        Usuario usuarioResultado = usuarioRepository.save(usuario);

        assertAll(
            () -> assertNotNull(usuarioResultado),
            () -> assertEquals(Usuario.class, usuarioResultado.getClass()),
            () -> assertEquals(usuario.getCPF(), usuarioResultado.getCPF()),
            () -> assertEquals(usuario.getEmail(), usuarioResultado.getEmail()),
            () -> assertEquals(usuario.getNome(), usuarioResultado.getNome()),
            () -> assertEquals(usuario.getDataNascimento(), usuarioResultado.getDataNascimento())
        );
    }
}
