package com.github.welblade.controleveiculos.repository.veiculo_usuario;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import com.github.welblade.controleveiculos.model.usuario.Usuario;
import com.github.welblade.controleveiculos.model.veiculo.Veiculo;
import com.github.welblade.controleveiculos.model.veiculo_usuario.VeiculoUsuario;
import com.github.welblade.controleveiculos.repository.usuario.UsuarioRepository;
import com.github.welblade.controleveiculos.repository.veiculo.VeiculoRepository;
import com.github.welblade.controleveiculos.util.DateConvertUtil;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

@SpringBootTest
public class VeiculoUsuarioRepositoryTest {
    @Autowired
    VeiculoRepository veiculoRepository;

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    VeiculoUsuarioRepository veiculoUsuarioRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    @Transactional
    @Rollback(true)
    public void quandoCadastrarRetornarObjeto(){

        Date data = DateConvertUtil.convertToDate("1979-10-05");
        
        Usuario usuario = new Usuario()
            .setCPF("91190098792")
            .setEmail("mariabeneditabarbosa.mariabeneditabarbosa@cntbrasil.com.br")
            .setNome("Maria Benedita Barbosa")
            .setDataNascimento(data);
                
        
        Usuario usuarioResultado = usuarioRepository.save(usuario);
        
        Veiculo veiculo = new Veiculo()
            .setMarca("Hyundai")
            .setModelo("Accent LS 4p")
            .setAno("1995")
            .setValor("R$ 4.799,00");    
        
        Veiculo veiculoResultado = veiculoRepository.save(veiculo);
        
        VeiculoUsuario veiculoProprietario = new VeiculoUsuario();
        veiculoProprietario
                .setProprietario(
                    usuarioResultado
                )
                .setVeiculo(
                    veiculoResultado
                );
        VeiculoUsuario resultado = veiculoUsuarioRepository.save(veiculoProprietario);

        assertAll(
            () -> assertNotNull(resultado),
            () -> assertEquals(VeiculoUsuario.class, resultado.getClass()),
            () -> assertEquals(usuarioResultado, resultado.getProprietario()),
            () -> assertEquals(veiculoResultado, resultado.getVeiculo())
        );
    }
}

