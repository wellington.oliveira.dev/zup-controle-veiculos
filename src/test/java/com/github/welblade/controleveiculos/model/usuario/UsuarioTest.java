package com.github.welblade.controleveiculos.model.usuario;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Calendar;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.TestComponent;

@TestComponent
public class UsuarioTest {
    
    @Test
    public void quandoCriadoDevePossuirAtributosCorretos(){
        Usuario usuario = new Usuario();
        usuario.setCPF("91190098792");
        usuario.setEmail("mariabeneditabarbosa.mariabeneditabarbosa@cntbrasil.com.br");
        usuario.setNome("Maria Benedita Barbosa");

        Calendar data = Calendar.getInstance();
        data.set(Calendar.YEAR, 1979);
        data.set(Calendar.MONTH, 10);
        data.set(Calendar.DAY_OF_MONTH, 5);

        usuario.setDataNascimento(data.getTime());

        assertAll(
            () -> assertEquals("91190098792", usuario.getCPF()),
            () -> assertEquals("mariabeneditabarbosa.mariabeneditabarbosa@cntbrasil.com.br", usuario.getEmail()),
            () -> assertEquals("Maria Benedita Barbosa", usuario.getNome()),
            () -> assertEquals(data.getTime(), usuario.getDataNascimento())
        );
    }
    
}
