package com.github.welblade.controleveiculos.model.veiculo_usuario;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Calendar;

import com.github.welblade.controleveiculos.model.usuario.Usuario;
import com.github.welblade.controleveiculos.model.veiculo.Veiculo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.TestComponent;

@TestComponent
public class VeiculoUsuarioTest {
    @Test
    public void aoCriarDeveConterPropriedadesCorretas(){
        Veiculo veiculo = new Veiculo();
        veiculo.setId(1);
        veiculo.setMarca("VW - VolksWagen");
        veiculo.setModelo("AMAROK High.CD 2.0 16V TDI 4x4 Dies. Aut");
        veiculo.setAno("2014");
        veiculo.setValor("R$ 102.451,00");

        Usuario usuario = new Usuario();
        usuario.setCPF("91190098792");
        usuario.setEmail("mariabeneditabarbosa.mariabeneditabarbosa@cntbrasil.com.br");
        usuario.setNome("Maria Benedita Barbosa");

        Calendar data = Calendar.getInstance();
        data.set(Calendar.YEAR, 1979);
        data.set(Calendar.MONTH, 10);
        data.set(Calendar.DAY_OF_MONTH, 5);

        usuario.setDataNascimento(data.getTime());


        VeiculoUsuario veiculoUsuario = new VeiculoUsuario();
        veiculoUsuario.setProprietario(usuario);
        veiculoUsuario.setVeiculo(veiculo);
        
        assertAll(
            () -> assertEquals(usuario, veiculoUsuario.getProprietario()),
            () -> assertEquals(veiculo, veiculoUsuario.getVeiculo())
        );
    }
    
}
