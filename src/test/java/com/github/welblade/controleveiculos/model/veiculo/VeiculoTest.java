package com.github.welblade.controleveiculos.model.veiculo;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.TestComponent;

@TestComponent
public class VeiculoTest {
    @Test
    public void quandoCriadoDevePossuirAtributosCorretos(){
        Veiculo veiculo = new Veiculo();
        veiculo.setId(1);
        veiculo.setMarca("VW - VolksWagen");
        veiculo.setModelo("AMAROK High.CD 2.0 16V TDI 4x4 Dies. Aut");
        veiculo.setAno("2014");
        veiculo.setValor("R$ 102.451,00");

        assertAll(
            () -> assertEquals(1, veiculo.getId()),
            () -> assertEquals("VW - VolksWagen", veiculo.getMarca()),
            () -> assertEquals("AMAROK High.CD 2.0 16V TDI 4x4 Dies. Aut", veiculo.getModelo()),
            () -> assertEquals("2014", veiculo.getAno()),
            () -> assertEquals("R$ 102.451,00", veiculo.getValor())
        );
    }    
}
