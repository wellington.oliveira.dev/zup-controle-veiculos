DROP TABLE IF EXISTS veiculo_usuario;
DROP TABLE IF EXISTS veiculo;
DROP TABLE IF EXISTS usuarios;

CREATE TABLE usuarios (
  cpf VARCHAR(11) PRIMARY KEY NOT NULL,
  email VARCHAR(250) NOT NULL,
  nome VARCHAR(250) NOT NULL,
  data_nascimento DATE DEFAULT NULL,
  UNIQUE KEY usuario_cpf_UNIQUE (cpf),
  UNIQUE KEY usuario_email_UNIQUE (email)  
);

CREATE TABLE veiculos (
  id INT AUTO_INCREMENT PRIMARY KEY,
  marca VARCHAR(250) NOT NULL,
  modelo VARCHAR(250) NOT NULL,
  ano VARCHAR(4) DEFAULT '1886',
  valor VARCHAR(250) DEFAULT 'R$ 0,00' 
);

CREATE TABLE veiculos_usuario (
    id INT AUTO_INCREMENT PRIMARY KEY,
    cpf_proprietario VARCHAR(11) NOT NULL,
    id_veiculo INT NOT NULL,
    CONSTRAINT fk_cpf_proprietario FOREIGN KEY(cpf_proprietario) REFERENCES usuarios(cpf),
    CONSTRAINT fk_id_veiculo FOREIGN KEY(id_veiculo) REFERENCES veiculos(id)
);

INSERT INTO veiculos (marca, modelo, ano, valor) VALUES
  ('VW - VolksWagen', 'AMAROK High.CD 2.0 16V TDI 4x4 Dies. Aut', '2014', 'R$ 102.451,00'),
  ('Hyundai', 'Accent LS 4p', '1995', 'R$ 4.799,00'),
  ('Cadillac', 'Seville 4.6', '2014', 'R$ 64.361,00');

INSERT INTO usuarios (cpf, email, nome, data_nascimento) VALUES
  ('55980294058', 'jana.eduarda@example.com', 'Joana Eduarda Caldeira', '1953-02-16'),
  ('52673222022', 'nathan.dan@example.com', 'Nathan Severino Danilo Alves', '2004-12-31'),
  ('75327045056', 'nat.pri@example.com', 'Natália Priscila Gabriela', '1999-01-15');

INSERT INTO veiculos_usuario (cpf_proprietario, id_veiculo) VALUES
  ('55980294058', (SELECT id FROM veiculos WHERE marca='VW - VolksWagen')),
  ('55980294058', (SELECT id FROM veiculos WHERE marca='Hyundai')),
  ('52673222022', (SELECT id FROM veiculos WHERE marca='Hyundai')),
  ('75327045056', (SELECT id FROM veiculos WHERE marca='Cadillac'));
