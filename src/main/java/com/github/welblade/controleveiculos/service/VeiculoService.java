package com.github.welblade.controleveiculos.service;

import javax.validation.Valid;

import com.github.welblade.controleveiculos.dto.model.veiculo.VeiculoDto;
import com.github.welblade.controleveiculos.model.veiculo.Veiculo;


public interface VeiculoService {

    Veiculo registrar(@Valid VeiculoDto veiculo);

}
