package com.github.welblade.controleveiculos.service;

import com.github.welblade.controleveiculos.dto.model.veiculo.VeiculoDto;
import com.github.welblade.controleveiculos.feign.VeiculoTabelaFipe;

public interface TabelaFipeService {
    public String getValorVeiculo(VeiculoDto veiculoDto);
    public VeiculoTabelaFipe getVeiculoTabelaFipe(VeiculoDto veiculoDto);
}
