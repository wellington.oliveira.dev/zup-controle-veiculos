package com.github.welblade.controleveiculos.service;

import java.util.List;

import com.github.welblade.controleveiculos.dto.model.veiculo.VeiculoDto;
import com.github.welblade.controleveiculos.exception.ApiFipeListaDeMarcasVaziasException;
import com.github.welblade.controleveiculos.exception.ApiFipeNenhumaMarcaEncontradaException;
import com.github.welblade.controleveiculos.exception.ApiFipeNenhumModeloEncontradoException;
import com.github.welblade.controleveiculos.exception.ApiFipeVeiculoNaoEncontradoException;
import com.github.welblade.controleveiculos.feign.ApiFipeClient;
import com.github.welblade.controleveiculos.feign.VeiculoAno;
import com.github.welblade.controleveiculos.feign.VeiculoMarca;
import com.github.welblade.controleveiculos.feign.VeiculoModelo;
import com.github.welblade.controleveiculos.feign.VeiculoModeloResponse;
import com.github.welblade.controleveiculos.feign.VeiculoTabelaFipe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component 
@Service
public class TabelaFipeServiceImpl implements TabelaFipeService{
    @Autowired
    ApiFipeClient tabelaFipe;

    public String getValorVeiculo(VeiculoDto veiculoDto){
        return getVeiculoTabelaFipe(veiculoDto).Valor;
    }

    @Override
    public VeiculoTabelaFipe getVeiculoTabelaFipe(VeiculoDto veiculoDto) {
       return encontrarCodigoMarca(veiculoDto);
    }

    private VeiculoTabelaFipe encontrarCodigoMarca(VeiculoDto veiculoDto){
        List<VeiculoMarca> marcas = tabelaFipe.retonaMarcas();
        if(marcas.isEmpty()){
            throw new ApiFipeListaDeMarcasVaziasException(
                "Houve um problema ao consultar as marcas Tabela FIPE."
            );
        }

        final var nomeMarca = veiculoDto.getMarca();

        VeiculoMarca[] marcasFiltradas = marcas.stream()
            .filter(
                marca -> marca.nome.contains(nomeMarca)
            ).toArray(VeiculoMarca[]::new);
        
        if(marcasFiltradas.length == 0){
            throw new ApiFipeNenhumaMarcaEncontradaException(
                "Nenhuma marca \""+ nomeMarca +"\" encontrada." +
                "Consulte a Tabela FIPE para encontrar o nome corret da marca."
            );
        }

        for(var i = 0; i < marcasFiltradas.length; i++){
            var marca = marcasFiltradas[i];
            try {
                return encontrarCodigoModelo(marca.codigo, veiculoDto);
            } catch (ApiFipeNenhumModeloEncontradoException e) {
                if(i == marcasFiltradas.length - 1){
                    throw e;
                }
            }
            
        }
        return null;
    }

    private VeiculoTabelaFipe encontrarCodigoModelo(String codMarca, VeiculoDto veiculoDto) {
        VeiculoModeloResponse response = tabelaFipe.retonaModelos(codMarca);
        if(response.modelos.isEmpty()){
            throw new ApiFipeNenhumModeloEncontradoException(
                "Nenhum modelo desta marca encontrado."+
                "Consulte a Tabela FIPE para encontrar o modelo correto."
            );
        }

        final var nomeModelo = veiculoDto.getModelo();

        VeiculoModelo[] modelosFiltrados = response.modelos.stream()
            .filter(
                modelo -> modelo.nome.contains(nomeModelo)
            )
            .toArray(VeiculoModelo[]::new);
    
        if(modelosFiltrados.length == 0){
            throw new ApiFipeNenhumModeloEncontradoException(
                "Nenhum modelo com nome \"" + nomeModelo + "\" encontrado. " +
                "Consulte a Tabela FIPE para encontrar o modelo correto."
            );
        }

        for(var i = 0; i < modelosFiltrados.length; i++){
            var modelo = modelosFiltrados[i];
            try {
                return encontrarCodigoAno(codMarca, modelo.codigo, veiculoDto);
            } catch (ApiFipeNenhumModeloEncontradoException e) {
                if(i == modelosFiltrados.length - 1){
                    throw e;
                }
            }
        }
        return null;
    }

    private VeiculoTabelaFipe encontrarCodigoAno(String codMarca, String codModelo, VeiculoDto veiculoDto) {
        List<VeiculoAno> anos = tabelaFipe.retonaAnos(codMarca, codModelo);
        if(anos.isEmpty()){
            throw new ApiFipeNenhumModeloEncontradoException(
                "Nenhum ano deste modelo encontrado."+
                "Consulte a Tabela FIPE para encontrar o modelo e ano correto."
            );
        }
        final var anoValor = veiculoDto.getAno();

        VeiculoAno[] anosFiltrados = anos.stream()
            .filter(
                elemento -> elemento.nome.contains(anoValor)
            )
            .toArray(VeiculoAno[]::new);

        if(anosFiltrados.length == 0){
            throw new ApiFipeNenhumModeloEncontradoException(
                "Nenhum modelo de ano " + anoValor + " encontrado."+
                "Consulte a Tabela FIPE para encontrar o modelo e ano correto."
                
            );
        }

        for(var i = 0; i < anosFiltrados.length; i++){
            var ano = anosFiltrados[i];
            try {
                return econtrarVeiculoTabelaFipe(codMarca, codModelo, ano.codigo);
            } catch (ApiFipeVeiculoNaoEncontradoException e) {
                if(i == anosFiltrados.length - 1){
                    throw e;
                }
            }
        }
        return null;
    }

    private VeiculoTabelaFipe econtrarVeiculoTabelaFipe(String codMarca,  String codModelo,  String codAno) {

        VeiculoTabelaFipe veiculo = tabelaFipe
            .retonaTabelaFipe( codMarca, codModelo, codAno);

        if(veiculo == null){
            throw new ApiFipeVeiculoNaoEncontradoException(
                "Nenhum veículo encontrado na Tabela FIPE. (" 
                + codMarca + ", " + codModelo + ", " + codAno + ")"
            );
        }
        return veiculo;
    }
}
