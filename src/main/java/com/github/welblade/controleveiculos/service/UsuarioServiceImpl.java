package com.github.welblade.controleveiculos.service;

import javax.validation.Valid;

import com.github.welblade.controleveiculos.controller.response.UsuarioResponse;
import com.github.welblade.controleveiculos.dto.mapper.UsuarioMapper;
import com.github.welblade.controleveiculos.dto.mapper.VeiculoUsuarioMapper;
import com.github.welblade.controleveiculos.dto.model.usuario.UsuarioDto;
import com.github.welblade.controleveiculos.exception.CPFUsuarioNaoCadastradoException;
import com.github.welblade.controleveiculos.model.usuario.Usuario;
import com.github.welblade.controleveiculos.repository.usuario.UsuarioRepository;
import com.github.welblade.controleveiculos.util.DateConvertUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Component 
@Service
@Validated
public class UsuarioServiceImpl implements UsuarioService {
    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public UsuarioDto registrar(@Valid UsuarioDto usuarioDto) {
        Usuario usuario = UsuarioMapper.toEntity(usuarioDto);
        Usuario usarioResultado = usuarioRepository.save(usuario);
        return UsuarioMapper.toDto(usarioResultado);
    }
    
    @Override
    public UsuarioResponse acharUmUsuario(String cpf){
        UsuarioResponse response = new UsuarioResponse();

        Usuario usuario = usuarioRepository.findById(cpf)
            .orElseThrow(CPFUsuarioNaoCadastradoException::new);

        response.setCPF(usuario.getCPF())
            .setEmail(usuario.getEmail())
            .setNome(usuario.getNome())
            .setDataNascimento(
                DateConvertUtil.convertToString(usuario.getDataNascimento())
            );
        usuario.getVeiculos().size();
        System.out.println("TESTE :: " + usuario.getVeiculos());
        for(var veiculo : usuario.getVeiculos()){
            response.getVeiculos()
                .add(VeiculoUsuarioMapper.toDto(veiculo));
        }

        return response;
    }

}
