package com.github.welblade.controleveiculos.service;

import javax.validation.Valid;

import com.github.welblade.controleveiculos.controller.response.UsuarioResponse;
import com.github.welblade.controleveiculos.dto.model.usuario.UsuarioDto;

public interface UsuarioService {

    UsuarioDto registrar(@Valid UsuarioDto usuario);
    UsuarioResponse acharUmUsuario(String cpf);

}
