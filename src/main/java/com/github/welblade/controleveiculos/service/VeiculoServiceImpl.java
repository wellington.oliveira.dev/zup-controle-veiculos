package com.github.welblade.controleveiculos.service;


import java.util.List;

import javax.validation.Valid;

import com.github.welblade.controleveiculos.dto.mapper.VeiculoMapper;
import com.github.welblade.controleveiculos.dto.model.veiculo.VeiculoDto;
import com.github.welblade.controleveiculos.exception.CPFUsuarioNaoCadastradoException;
import com.github.welblade.controleveiculos.exception.VeiculoJaCadastradoException;
import com.github.welblade.controleveiculos.feign.VeiculoTabelaFipe;
import com.github.welblade.controleveiculos.model.usuario.Usuario;
import com.github.welblade.controleveiculos.model.veiculo.Veiculo;
import com.github.welblade.controleveiculos.model.veiculo_usuario.VeiculoUsuario;
import com.github.welblade.controleveiculos.repository.usuario.UsuarioRepository;
import com.github.welblade.controleveiculos.repository.veiculo.VeiculoRepository;
import com.github.welblade.controleveiculos.repository.veiculo_usuario.VeiculoUsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Component
@Validated
@Service
public class VeiculoServiceImpl implements VeiculoService{
    @Autowired 
    VeiculoRepository veiculoRepository;

    @Autowired 
    UsuarioRepository usuarioRepository;

    @Autowired 
    VeiculoUsuarioRepository veiculoUsuarioRepository;

    @Autowired
    TabelaFipeService tabelaFipe;

    @Override
    public Veiculo registrar(@Valid VeiculoDto veiculoDto) {
        
        if(veiculoDto.getCPF() != null){
            return registrarVeiculoComUsuario(veiculoDto);
        }

        return registrarVeiculoSemUsuario(veiculoDto);
    }

    private Veiculo registrarVeiculoSemUsuario(VeiculoDto veiculoDto){
        if(encontrarVeiculoNoBanco(veiculoDto) != null) 
            throw new VeiculoJaCadastradoException(
                "Este veículo já está cadastrado em nosso sistema"
            );
        
        return registraVeiculo(veiculoDto);
    }

    private Veiculo registrarVeiculoComUsuario(VeiculoDto veiculoDto){

        Usuario usuario = usuarioRepository.findById(
                veiculoDto.getCPF()
        ).orElseThrow(CPFUsuarioNaoCadastradoException::new);

        Veiculo veiculo = encontrarVeiculoNoBanco(veiculoDto);

        if(veiculo == null) {
            veiculo = registraVeiculo(veiculoDto);
        }
        
        registrarVeiculoUsuario(usuario, veiculo);
        return veiculo;
    }

    private Veiculo encontrarVeiculoNoBanco(VeiculoDto veiculoDto){
        List<Veiculo> veiculos = veiculoRepository.findByMarcaLikeAndModeloLikeAndAno(
            veiculoDto.getMarca(), 
            veiculoDto.getModelo(), 
            veiculoDto.getAno()
        );
        if(veiculos.size() > 0){
            return veiculos.get(0);
        }
        return null;
    }

    private void registrarVeiculoUsuario(Usuario usuario, Veiculo veiculo) {
        VeiculoUsuario vUsuario = new VeiculoUsuario()
            .setProprietario(usuario)
            .setVeiculo(veiculo);

        veiculoUsuarioRepository.save(vUsuario);
    }

    private Veiculo registraVeiculo(VeiculoDto veiculoDto) {

        VeiculoTabelaFipe tabela = tabelaFipe.getVeiculoTabelaFipe(veiculoDto);
    
        veiculoDto.setMarca(tabela.Marca);
        veiculoDto.setModelo(tabela.Modelo);
        veiculoDto.setValor(tabela.Valor);

        Veiculo veiculo = VeiculoMapper.toEntity(veiculoDto);
        return veiculoRepository.save(veiculo);
    }
}
