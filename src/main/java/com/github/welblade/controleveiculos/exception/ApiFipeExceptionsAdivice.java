package com.github.welblade.controleveiculos.exception;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiFipeExceptionsAdivice {

    @ExceptionHandler(ApiFipeListaDeMarcasVaziasException.class)
    public ResponseEntity<Map<String, List<String>>> ApiFipeListaDeMarcasVaziasHandler(ApiFipeListaDeMarcasVaziasException exception){
        return genericHandler(exception, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ApiFipeNenhumaMarcaEncontradaException.class)
    public ResponseEntity<Map<String, List<String>>> ApiFipeNenhumaMarcaEncontradaHandler(ApiFipeNenhumaMarcaEncontradaException exception){
        return genericHandler(exception, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ApiFipeNenhumModeloEncontradoException.class)
    public ResponseEntity<Map<String, List<String>>> ApiFipeNenhumModeloEncontradoHandler(ApiFipeNenhumModeloEncontradoException exception){
        return genericHandler(exception, HttpStatus.BAD_REQUEST);
    }

    
    @ExceptionHandler(ApiFipeVeiculoNaoEncontradoException.class)
    public ResponseEntity<Map<String, List<String>>> ApiFipeVeiculoNaoEncontradoHandler(ApiFipeVeiculoNaoEncontradoException exception){
        return genericHandler(exception, HttpStatus.BAD_REQUEST);
    }

    private <T> ResponseEntity<Map<String, List<String>>> genericHandler(Exception exception,  HttpStatus status){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        
        Map<String, List<String>> erro = new HashMap<>();
        erro.put("erro", new ArrayList<String>());
       
        erro.get("erro").add(exception.getMessage());
        return new ResponseEntity<Map<String, List<String>>>(erro, headers, status);
    }


}



