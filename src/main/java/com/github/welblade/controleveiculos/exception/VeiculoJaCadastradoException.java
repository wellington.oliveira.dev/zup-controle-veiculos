package com.github.welblade.controleveiculos.exception;

public class VeiculoJaCadastradoException extends RuntimeException {
    public VeiculoJaCadastradoException(String mensagem){
        super(mensagem);
    }
}
