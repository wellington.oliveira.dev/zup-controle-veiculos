package com.github.welblade.controleveiculos.exception;

public class ApiFipeListaDeMarcasVaziasException extends RuntimeException{
    public ApiFipeListaDeMarcasVaziasException(String mensagem){
        super(mensagem);
    }

}
