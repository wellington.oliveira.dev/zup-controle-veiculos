package com.github.welblade.controleveiculos.exception;

public class CPFUsuarioNaoCadastradoException extends RuntimeException{
    public CPFUsuarioNaoCadastradoException(){
        super("Não existe um usuário com este CPF cadastrado em nosso sistema, por favor cadastre-o primeiro.");
    }

}
