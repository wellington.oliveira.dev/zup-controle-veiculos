package com.github.welblade.controleveiculos.exception;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@ResponseStatus(HttpStatus.CONFLICT)
public class VeiculoJaCadastradoExceptionAdivice {
    @ExceptionHandler(VeiculoJaCadastradoException.class)
    public ResponseEntity<Map<String, List<String>>> VeiculoJaCadastradoHandler(VeiculoJaCadastradoException exception) throws JsonProcessingException{
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        
        Map<String, List<String>> erro = new HashMap<>();
        erro.put("erro", new ArrayList<String>());
       
        erro.get("erro").add(exception.getMessage());
        return new ResponseEntity<Map<String, List<String>>>(erro, headers, HttpStatus.CONFLICT);
    } 
    
}
