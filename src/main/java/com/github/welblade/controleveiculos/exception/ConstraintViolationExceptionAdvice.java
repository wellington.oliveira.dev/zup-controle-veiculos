package com.github.welblade.controleveiculos.exception;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ConstraintViolationExceptionAdvice {
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Map<String, List<String>>> constraintViolationHandler(ConstraintViolationException exception) throws JsonProcessingException{
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        
        Map<String, List<String>> erro = new HashMap<>();
        erro.put("erro", new ArrayList<String>());
       
        for (ConstraintViolation<?> violation : exception.getConstraintViolations()) {
            erro.get("erro").add(violation.getMessage());
        }
        return new ResponseEntity<Map<String, List<String>>>(erro, headers, HttpStatus.BAD_REQUEST);
    } 
}
