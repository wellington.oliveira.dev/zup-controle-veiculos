package com.github.welblade.controleveiculos.exception;

public class ApiFipeNenhumModeloEncontradoException extends RuntimeException{
    public ApiFipeNenhumModeloEncontradoException(String mensagem){
        super(mensagem);
    }
}
