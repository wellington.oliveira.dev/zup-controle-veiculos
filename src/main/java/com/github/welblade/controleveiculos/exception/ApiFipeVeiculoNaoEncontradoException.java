package com.github.welblade.controleveiculos.exception;

public class ApiFipeVeiculoNaoEncontradoException extends RuntimeException{
    public ApiFipeVeiculoNaoEncontradoException(String mensagem){
        super(mensagem);
    }
}
