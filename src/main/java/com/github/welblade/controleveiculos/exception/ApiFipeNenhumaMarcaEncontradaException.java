package com.github.welblade.controleveiculos.exception;

public class ApiFipeNenhumaMarcaEncontradaException extends RuntimeException {
    public ApiFipeNenhumaMarcaEncontradaException(String mensagem){
        super(mensagem);
    }
}
