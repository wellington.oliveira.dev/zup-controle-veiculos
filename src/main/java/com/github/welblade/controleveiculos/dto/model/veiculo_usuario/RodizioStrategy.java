package com.github.welblade.controleveiculos.dto.model.veiculo_usuario;

import java.time.DayOfWeek;

public interface RodizioStrategy {
    public DayOfWeek getDiaRodizio(String ano);
}
