package com.github.welblade.controleveiculos.dto.model.veiculo_usuario;

import java.time.DayOfWeek;

public class RegrasRodizio implements RodizioStrategy{

    @Override
    public DayOfWeek getDiaRodizio(String ano) {
        final int ultimoDigito = Integer.valueOf(
            ano.substring(ano.length() - 1)
        );
        if(ultimoDigito <= 1) return DayOfWeek.MONDAY;   // Segunda-feira
        if(ultimoDigito <= 3) return DayOfWeek.TUESDAY;  // Terça-feira
        if(ultimoDigito <= 5) return DayOfWeek.WEDNESDAY;// Quarta-feira
        if(ultimoDigito <= 7) return DayOfWeek.THURSDAY; // Quinta-feira
        return DayOfWeek.FRIDAY;                         // Sexta-feira
    }


}
