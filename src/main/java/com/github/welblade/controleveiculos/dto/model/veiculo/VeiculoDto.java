package com.github.welblade.controleveiculos.dto.model.veiculo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CPF;

public class VeiculoDto {

    @CPF
    private String cpf;

    @NotNull
    @NotBlank
    @Length(min=1, max=250)
    private String marca;

    @NotNull
    @NotBlank
    @Length(min=1, max=250)
    private String modelo;

    @NotNull
    @NotBlank
    @Length(min=1, max=4)
    private String ano;

    private String valor;

    public VeiculoDto setCPF(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public VeiculoDto setMarca(String marca) {
        this.marca = marca;
        return this;
    }

    public VeiculoDto setModelo(String modelo) {
        this.modelo = modelo;
        return this;
    }

    public VeiculoDto setAno(String ano) {
        this.ano = ano;
        return this;
    }

    public VeiculoDto setValor(String valor) {
        this.valor = valor;
        return this;
    }

    public String getCPF() {
        return cpf;
    }

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public String getAno() {
        return ano;
    }

    public String getValor() {
        return valor;
    }

}
