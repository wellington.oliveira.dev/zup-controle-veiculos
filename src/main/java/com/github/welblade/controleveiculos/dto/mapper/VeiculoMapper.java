package com.github.welblade.controleveiculos.dto.mapper;

import com.github.welblade.controleveiculos.dto.model.veiculo.VeiculoDto;
import com.github.welblade.controleveiculos.model.veiculo.Veiculo;

public class VeiculoMapper {

    public static Veiculo toEntity(VeiculoDto veiculoDto) {
        return new Veiculo()
            .setMarca(veiculoDto.getMarca())
            .setModelo(veiculoDto.getModelo())
            .setAno(veiculoDto.getAno())
            .setValor(veiculoDto.getValor());
    }

    public static VeiculoDto toDto(Veiculo veiculo) {
        return new VeiculoDto()
            .setMarca(veiculo.getMarca())
            .setModelo(veiculo.getModelo())
            .setAno(veiculo.getAno())
            .setValor(veiculo.getValor());
    }
}
