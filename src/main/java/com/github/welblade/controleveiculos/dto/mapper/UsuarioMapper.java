package com.github.welblade.controleveiculos.dto.mapper;

import com.github.welblade.controleveiculos.dto.model.usuario.UsuarioDto;
import com.github.welblade.controleveiculos.model.usuario.Usuario;
import com.github.welblade.controleveiculos.util.DateConvertUtil;

import org.springframework.stereotype.Component;

@Component("usuarioMapper")
public class UsuarioMapper {

    public static UsuarioDto toDto(Usuario usuario) {
        final var dataNascimento = DateConvertUtil.convertToString(
            usuario.getDataNascimento()
        );
        return new UsuarioDto()
                .setCPF(usuario.getCPF())
                .setEmail(usuario.getEmail())
                .setNome(usuario.getNome())
                .setDataNascimento(dataNascimento);
    }

    public static Usuario toEntity(UsuarioDto usuarioDto) {
        final var dataNascimento = DateConvertUtil.convertToDate(
            usuarioDto.getDataNascimento()
        );
        return new Usuario()
                .setCPF(usuarioDto.getCPF())
                .setEmail(usuarioDto.getEmail())
                .setNome(usuarioDto.getNome())
                .setDataNascimento(dataNascimento);
    }


}
