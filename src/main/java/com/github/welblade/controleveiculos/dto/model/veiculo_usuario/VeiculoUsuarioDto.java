package com.github.welblade.controleveiculos.dto.model.veiculo_usuario;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;

import com.github.welblade.controleveiculos.util.DateConvertUtil;

public class VeiculoUsuarioDto {

    private String marca;
    private String modelo;
    private String ano;
    private String valor;
    private boolean isRodizioAtivo;

    public VeiculoUsuarioDto setMarca(String marca) {
        this.marca = marca;
        return this;
    }

    public VeiculoUsuarioDto setModelo(String modelo) {
        this.modelo = modelo;
        return this;
    }

    public VeiculoUsuarioDto setAno(String ano) {
        this.ano = ano;
        return this;
    }

    public VeiculoUsuarioDto setValor(String valor) {
        this.valor = valor;
        return this;
    }

    public VeiculoUsuarioDto setIsRodizioAtivo(Date hoje, RodizioStrategy estrategia) {
        DayOfWeek diaRodizio = estrategia.getDiaRodizio(ano);
        LocalDate data = DateConvertUtil.convertToLocalDate(hoje);
        this.isRodizioAtivo = data.getDayOfWeek() == diaRodizio;
        return this;
    }


    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public String getAno() {
        return ano;
    }

    public String getValor() {
        return valor;
    }


    public boolean getIsRodizioAtivo() {
        return isRodizioAtivo;
    }
    
}
