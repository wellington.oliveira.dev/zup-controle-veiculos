package com.github.welblade.controleveiculos.dto.model.usuario;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.welblade.controleveiculos.validator.CpfUnico;
import com.github.welblade.controleveiculos.validator.EmailUnico;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CPF;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsuarioDto {
    @NotNull(message = "O CPF não pode ser um valor nulo.")
    @NotBlank(message = "O CPF nao pode estar em branco.")
    @CPF(message = "CPF Inválido, o cpf deve ser formado apenas por números.")
    @Length(max=11, min=11, message="O comprimento do CPF é de 11.")
    @CpfUnico
    private String cpf;

    @NotNull(message = "O Email não pode ser um valor nulo.")
    @NotBlank(message = "O Email não pode estar em branco.")
    @Email(message= "O email não possui um formato válido.")
    @Length(max=250, min=3, message="O comprimento do Email deve ser entre 3 e 250.")
    @EmailUnico
    private String email;

    @NotNull(message = "O Nome não pode ser um valor nulo.")
    @NotBlank(message = "O Nome não pode estar em branco.")
    @Length(max=250, min=3, message="O comprimento do Nome deve ser entre 3 e 250.")
    private String nome;

    @NotNull(message = "A data de nascimento não pode ser um valor nulo.")
    @NotBlank(message = "A data de nascimento não pode estar em branco.")
    @Pattern(regexp = "^([0-2]{1}[0-9]{3})-([0]{1}[0-9]{1}|[1]{1}[0-2]{1})-([0-2]{1}[0-9]{1}|[3]{1}[0-1]{1})$", message="A data deve ter o formato YYYY-MM-DD")
    private String dataNascimento;

    public UsuarioDto setCPF(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public UsuarioDto setEmail(String email) {
        this.email = email;
        return this;
    }

    public UsuarioDto setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public UsuarioDto setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
        return this;
    }

    public String getCPF() {
        return cpf;
    }

    public String getEmail() {
        return email;
    }

    public String getNome() {
        return nome;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

}
