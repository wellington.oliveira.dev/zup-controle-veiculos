package com.github.welblade.controleveiculos.dto.mapper;

import java.util.Date;

import com.github.welblade.controleveiculos.dto.model.veiculo_usuario.RegrasRodizio;
import com.github.welblade.controleveiculos.dto.model.veiculo_usuario.VeiculoUsuarioDto;
import com.github.welblade.controleveiculos.model.veiculo_usuario.VeiculoUsuario;

public class VeiculoUsuarioMapper {

    public static VeiculoUsuarioDto toDto(VeiculoUsuario veiculoUsuario) {

        VeiculoUsuarioDto dto = new VeiculoUsuarioDto();
        dto.setMarca(veiculoUsuario.getVeiculo().getMarca())
            .setModelo(veiculoUsuario.getVeiculo().getModelo())
            .setAno(veiculoUsuario.getVeiculo().getAno())
            .setValor(veiculoUsuario.getVeiculo().getValor());
        dto.setIsRodizioAtivo(new Date(), new RegrasRodizio());
        return dto;
    }

}
