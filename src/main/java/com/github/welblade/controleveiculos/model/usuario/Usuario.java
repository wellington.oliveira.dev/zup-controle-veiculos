package com.github.welblade.controleveiculos.model.usuario;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.github.welblade.controleveiculos.model.veiculo_usuario.VeiculoUsuario;

@Entity
@Table(name = "usuarios")
public class Usuario {
    @Id
    private String cpf;
    private String email;
    private String nome;
    private Date dataNascimento;

    @OneToMany
    @JoinColumn(name = "cpf_proprietario", referencedColumnName="cpf")
    private List<VeiculoUsuario> veiculos;

    public Usuario setCPF(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public Usuario setEmail(String email) {
        this.email = email;
        return this;
    }

    public Usuario setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public Usuario setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
        return this;
    }

    public Usuario setVeiculos(List<VeiculoUsuario> veiculos){
        this.veiculos = veiculos;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public String getNome() {
        return nome;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public String getCPF() {
        return cpf;
    }

    public List<VeiculoUsuario> getVeiculos(){
        return veiculos;
    }

}
