package com.github.welblade.controleveiculos.model.veiculo_usuario;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.github.welblade.controleveiculos.model.usuario.Usuario;
import com.github.welblade.controleveiculos.model.veiculo.Veiculo;

@Entity
@Table(name = "veiculos_usuario")
public class VeiculoUsuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cpf_proprietario", referencedColumnName="cpf")
    private Usuario proprietario;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="id_veiculo", referencedColumnName="id")
    private Veiculo veiculo;

    public VeiculoUsuario setId(int id) {
        this.id = id;
        return this;
    }

    public VeiculoUsuario setProprietario(Usuario usuario) {
        this.proprietario = usuario;
        return this;
    }

    public VeiculoUsuario setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
        return this;
    }

    public int getId() {
        return id;
    }

    public Usuario getProprietario(){
        return proprietario;
    }

    public Veiculo getVeiculo(){
        return veiculo;
    }
}