package com.github.welblade.controleveiculos.model.veiculo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.github.welblade.controleveiculos.model.veiculo_usuario.VeiculoUsuario;

@Entity
@Table(name = "veiculos")
public class Veiculo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String marca;
    private String modelo;
    private String ano;
    private String valor;

    @OneToMany
    @JoinColumn(name = "ID_VEICULO")
    private List<VeiculoUsuario> veiculos;

    public Veiculo setId(int id) {
        this.id = id;
        return this;
    }

    public Veiculo setMarca(String marca) {
        this.marca = marca;
        return this;
    }

    public Veiculo setModelo(String modelo) {
        this.modelo = modelo;
        return this;
    }

    public Veiculo setAno(String ano) {
        this.ano = ano;
        return this;
    }

    public Veiculo setValor(String valor) {
        this.valor = valor;
        return this;
    }

    public int getId() {
        return id;
    }

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public String getAno() {
        return ano;
    }

    public String getValor() {
        return valor;
    }

}
