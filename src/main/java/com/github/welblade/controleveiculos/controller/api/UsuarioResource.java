package com.github.welblade.controleveiculos.controller.api;

import com.github.welblade.controleveiculos.controller.response.UsuarioResponse;
import com.github.welblade.controleveiculos.dto.model.usuario.UsuarioDto;
import com.github.welblade.controleveiculos.service.UsuarioService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuario")
public class UsuarioResource {

    public UsuarioResource(UsuarioService service) {
        this.service = service;
    }

    private final UsuarioService service;
    
    @PostMapping(produces = "application/json; charset=utf-8")
    public ResponseEntity<Void> registrarUsuario(@RequestBody UsuarioDto usuarioDto){
        service.registrar(usuarioDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping(produces = "application/json; charset=utf-8", value = "/{cpf}")
    public ResponseEntity<UsuarioResponse> retornarUsuario(@PathVariable String cpf){
        UsuarioResponse response = service.acharUmUsuario(cpf);
        return new ResponseEntity<UsuarioResponse>(response,HttpStatus.OK );
    }
    
}
