package com.github.welblade.controleveiculos.controller.response;

import java.util.ArrayList;
import java.util.List;

import com.github.welblade.controleveiculos.dto.model.veiculo_usuario.VeiculoUsuarioDto;

public class UsuarioResponse {
    private String cpf;
    private String email;
    private String nome;
    private String dataNascimento;
    private List<VeiculoUsuarioDto> veiculos = new ArrayList<VeiculoUsuarioDto>();
    
    public UsuarioResponse setCPF(String cpf) {
        this.cpf = cpf;
        return this;
    }
    public UsuarioResponse setEmail(String email) {
        this.email = email;
        return this;
    }
    public UsuarioResponse setNome(String nome) {
        this.nome = nome;
        return this;
    }
    public UsuarioResponse setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
        return this;
    }
    public UsuarioResponse setVeiculos(List<VeiculoUsuarioDto> veiculos) {
        this.veiculos = veiculos;
        return this;
    }
    public String getCPF() {
        return cpf;
    }
    public String getEmail() {
        return email;
    }
    public String getNome() {
        return nome;
    }
    public String getDataNascimento() {
        return dataNascimento;
    }
    public List<VeiculoUsuarioDto> getVeiculos() {
        return veiculos;
    }

}
