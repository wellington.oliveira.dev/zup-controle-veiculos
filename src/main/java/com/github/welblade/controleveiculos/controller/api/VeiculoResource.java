package com.github.welblade.controleveiculos.controller.api;

import com.github.welblade.controleveiculos.dto.model.veiculo.VeiculoDto;
import com.github.welblade.controleveiculos.service.VeiculoService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/veiculo")
public class VeiculoResource {

    public VeiculoResource(VeiculoService service) {
        this.service = service;
    }

    private final VeiculoService service;
    
    @PostMapping(produces = "application/json; charset=utf-8")
    public ResponseEntity<Void> registrarUsuario(@RequestBody VeiculoDto veiculoDto){
        service.registrar(veiculoDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    
}
