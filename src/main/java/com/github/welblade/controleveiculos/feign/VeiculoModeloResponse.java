package com.github.welblade.controleveiculos.feign;

import java.util.List;

public class VeiculoModeloResponse {
    public List<VeiculoAno> anos;
    public List<VeiculoModelo> modelos;

}
