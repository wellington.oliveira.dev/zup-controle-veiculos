package com.github.welblade.controleveiculos.feign;

public class VeiculoTabelaFipe {
    public String Marca;
    public String Modelo;
    public String Valor;
    public int AnoModelo;
    public String Combustivel;
    public String CodigoFipe;
    public String MesReferencia;
    public String TipoVeiculo;
    public String SiglaCombustivel;
}
