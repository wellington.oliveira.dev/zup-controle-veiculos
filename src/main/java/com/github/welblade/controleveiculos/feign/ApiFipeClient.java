package com.github.welblade.controleveiculos.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


//https://parallelum.com.br/fipe/api/v1/carros/marcas
@FeignClient(value = "fipe", url = "https://parallelum.com.br/fipe/api/v1/carros")
public interface ApiFipeClient {
   // @RequestLine("GET /marcas")
    @RequestMapping(method = RequestMethod.GET, value = "/marcas")
    List<VeiculoMarca> retonaMarcas();

    // marcas/59/modelos
    //@RequestLine("GET /marcas/{codMarca}/modelos")
    @RequestMapping(method = RequestMethod.GET, value = "/marcas/{codMarca}/modelos")
    VeiculoModeloResponse retonaModelos(
        @PathVariable("codMarca") String codMarca
    );
    
    // marcas/59/modelos/5940/anos
   // @RequestLine("GET /marcas/{codMarca}/modelos/{codModelo}/anos")
    @RequestMapping(method = RequestMethod.GET, value = "/marcas/{codMarca}/modelos/{codModelo}/anos")
    List<VeiculoAno> retonaAnos(
        @PathVariable("codMarca") String codMarca, 
        @PathVariable("codModelo") String codModelo
    );

    // /marcas/59/modelos/5940/anos/2014-3
    @RequestMapping(method = RequestMethod.GET, value = "/marcas/{codMarca}/modelos/{codModelo}/anos/{codAno}")
    VeiculoTabelaFipe retonaTabelaFipe(
        @PathVariable("codMarca") String codMarca, 
        @PathVariable("codModelo") String codModelo,
        @PathVariable("codAno") String codAno
    );

}