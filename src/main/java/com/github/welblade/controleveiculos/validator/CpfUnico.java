package com.github.welblade.controleveiculos.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CpfUnicoValidator.class)
@Documented
public @interface CpfUnico {
    String message() default "Este CPF já está cadastrado em nosso sistema.";

    Class<?>[] groups() default { };
  
    Class<? extends Payload>[] payload() default { };
}