package com.github.welblade.controleveiculos.validator;


import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.github.welblade.controleveiculos.model.usuario.Usuario;
import com.github.welblade.controleveiculos.repository.usuario.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;

public class EmailUnicoValidator implements ConstraintValidator<EmailUnico, String>{
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        Optional<Usuario> usuarioVerificacao = usuarioRepository.findByEmail(email);
        
        if(usuarioVerificacao.isPresent()){
            return false;
        }
        return true;
    }

}

