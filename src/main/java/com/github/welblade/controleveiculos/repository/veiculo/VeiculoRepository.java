package com.github.welblade.controleveiculos.repository.veiculo;
import java.util.List;

import com.github.welblade.controleveiculos.model.veiculo.Veiculo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VeiculoRepository extends JpaRepository<Veiculo, Integer>{
    List<Veiculo> findByMarcaLikeAndModeloLikeAndAno(String marca, String modelo, String ano);

}