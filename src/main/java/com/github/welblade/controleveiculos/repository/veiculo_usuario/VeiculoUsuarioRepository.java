package com.github.welblade.controleveiculos.repository.veiculo_usuario;

import java.util.List;

import com.github.welblade.controleveiculos.model.veiculo_usuario.VeiculoUsuario;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VeiculoUsuarioRepository 
    extends JpaRepository<VeiculoUsuario, Integer> {
        List<VeiculoUsuario> findByVeiculoId(int id);
}