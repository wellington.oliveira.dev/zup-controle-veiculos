package com.github.welblade.controleveiculos.repository.usuario;

import java.util.Optional;

import com.github.welblade.controleveiculos.model.usuario.Usuario;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, String> {
    Optional<Usuario> findByEmail(String email);
}
