package com.github.welblade.controleveiculos.util;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DateConvertUtil {
    public static String convertToString(Date date){
        final var formatador = new SimpleDateFormat("yyyy-MM-dd");
        return formatador.format(date);
    }

    public static Date convertToDate(String date){
        final String[] valores = date.split("-");
        
        LocalDate dataLocal = LocalDate.of(
            Integer.decode(valores[0]), 
            Integer.decode(valores[1]), 
            Integer.decode(valores[2])
        );
                
        return Date.from(
            dataLocal.atStartOfDay(
                ZoneId.systemDefault()
            ).toInstant()
        );
    }

    public static LocalDate convertToLocalDate(Date date){
        String text = convertToString(date);
        return LocalDate.parse(text);
    }
}
